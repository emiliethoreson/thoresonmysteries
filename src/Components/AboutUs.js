import React from "react";

function AboutUs() {
    return (
        <div className="text-container about-us">
            <h5>
            Who are ES Mysteries? That's a puzzle you may never solve...<br /><br />
            Just kidding, we were too cheap to buy a domain & our reminder emails are sent from our personal accounts. We're Sue & Emilie, a mother-daughter duo. Our hobbies include syllacrostics, Agatha Christie novels, Nancy Drew desktop games, horror-themed escape rooms, and now custom-built scavenger hunts.
            </h5>
        </div>
    )
}

export default AboutUs;