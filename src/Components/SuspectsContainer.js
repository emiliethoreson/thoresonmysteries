import React, { useState, useEffect } from "react";
import { suspects } from "./SuspectsObject";
import parse from 'html-react-parser';
import "./Suspects.css";

function SuspectsContainer() {
    let [slideIndex, setSlideIndex] = useState(0);
    let [activeSuspect, setActiveSuspect] = useState(suspects[0]);
    let [suspectText, setSuspectText] = useState();

    const plusSlide = (n) => {
        if (slideIndex === suspects.length - 1 && n === 1) {
            setSlideIndex(0);
            setActiveSuspect(suspects[0])
            return;
        } else if (slideIndex === 0 && n === -1) {
            setSlideIndex(suspects.length - 1);
            setActiveSuspect(suspects[suspects.length - 1])
            return;
        }
        else {
            setSlideIndex(slideIndex += n);
        }
        for (const suspect in suspects) {
            if (suspects[suspect].key === slideIndex) {
                setActiveSuspect(suspects[suspect]);
                return;
            }
        }
    }
    useEffect(() => {
            setSuspectText(
            <div>
                <h5 className="suspect-name" style={{borderBottom : `4px solid ${activeSuspect['color']}`}}>{activeSuspect.name}</h5>
                <div className="bio_content">
                    <span className="identifier">
                        {Object.keys(activeSuspect)[3]}:
                    </span>
                    {activeSuspect['Bio']}
                </div>
                <div className="bio_content">
                    <span className="identifier">
                        {Object.keys(activeSuspect)[4]}:
                    </span>
                    {activeSuspect['Alibi']}
                </div>
                <div className="bio_content">
                    <span className="identifier">
                        {Object.keys(activeSuspect)[5]}:
                    </span>
                    {activeSuspect['Motive']}
                </div>
                <div className="bio_content">
                    <span className="identifier">
                        Who do you think did it?
                    </span>
                    {activeSuspect['Guess']}
                </div>
            </div>
            );
    }, [activeSuspect, slideIndex])
    return (
        <div className="suspects-container">
            <h4 className="suspects-title">Meet the Nine Suspects</h4>
            <div className="slideshow-container">
                <div className="prev" onClick={() => plusSlide(-1)}>&#10094;</div>
                <div className={"mySlides fade"}>
                    {parse(activeSuspect.image)}
                    {suspectText}
                </div>
                <div className="next" onClick={() => plusSlide(1)}>&#10095;</div>
            </div>
        </div>
    )
}

export default SuspectsContainer;