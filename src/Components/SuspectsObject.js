exports.suspects = [
    {
        key: 0,
        id: 'antonia',
        "name": "Antonia Munoz",
        "Bio": " The Stone family’s lawyer. Though she’s been known for her temper at work, she’s got a good eye for details and is known for putting her clients above all else. She’s at the house at the request of Mrs. Stone, who wants to make a change in the will.",
        "Alibi": " 'I was in the library downstairs. I’m fully willing to state in a court of law that I was not after the Stones’ money.'",
        "Motive": " She knows exactly what the gems are worth & who they will eventually be going to.",
        "Guess": " 'I wouldn’t trust any of these people.'",
        "color": "purple",
        "image": '<a href="https://imgur.com/mIreC3r"><img src="https://i.imgur.com/mIreC3r.png" title="source: imgur.com" /></a>'
    },
    {
        key: 1,
        id: 'sophie',
        "name": "Sophie Stone",
        "Bio": " The youngest of the Stone daughters. Kindergarten teacher for the neighboring district. Sweet, sometimes a little too giving.",
        "Alibi": " 'I would never put my family through this! I was in the hallway when I heard my mother scream & was the first to get to her.'",
        "Motive": " She could have pawned them off to support any of the many charities to which she’s made promises.",
        "Guess": " 'I don’t think anyone here is capable of stealing the gems, they must have been misplaced.'",
        "color": "pink",
        "image": '<a href="https://imgur.com/XtPf3w7"><img src="https://i.imgur.com/XtPf3w7.png" title="source: imgur.com" /></a>'
    },
    {
        key: 2,
        id: 'katherine',
        "name": "Katherine Stone",
        "Bio": " Eldest of the Stone daughters, 'Kat' is the wild child and spends more time travelling abroad than at home. She’ll pick up any odd job & spend the money as soon as she gets it.",
        "Alibi": " 'I was lounging on the back patio. As the oldest, the gems probably would have gone to me, so here’s hoping you find them.'",
        "Motive": " She might need some extra funding for her spontaneous lifestyle.",
        "Guess": " 'Never trust a lawyer, especially one who makes house calls.'",
        "color": "blue",
        "image": '<a href="https://imgur.com/rrac1Db"><img src="https://i.imgur.com/rrac1Db.png" title="source: imgur.com" /></a>'
    },
    {
        key: 3,
        id: 'leo',
        "name": "Leo Harrings",
        "Bio": " An artist who is at the house to put the finishing touches on a family portrait. He’s an old soul who plays guitar and sculpts. You know he really likes a girl when he writes poetry for her.",
        "Alibi": " 'I was in the family room painting all morning.'",
        "Motive": " He’s a starving artist. He could have been enticed by the beauty & figured the gems were worth something.",
        "Guess": " 'I guarantee you it was the middle daughter, Evie - the one in green. She had the worst attitude when sitting for the portrait, locking her up would do her some good.'",
        "color": "red",
        "image": '<a href="https://imgur.com/Q1L1vRX"><img src="https://i.imgur.com/Q1L1vRX.png" title="source: imgur.com" /></a>'
    },
    {
        key: 4,
        id: 'beau',
        "name": "Beau Barnette",
        "Bio": " The bumbling-but-beloved next door neighbor. Went to school with the Stone girls & knows the idiosyncrasies of the family. Ran over when he heard Mrs. Stone’s shrieks.",
        "Alibi": " 'I got here after they were stolen.'",
        "Motive": " Rumor has it his place is in foreclosure, maybe he was desperate to save his parents’ home.",
        "Guess": " 'Well it’s rude to point fingers, ‘course I don’t want to assume, it’s just maybe you ought to know that that gardener… well Mr. Stone believed in second chances, it’s just, that Morse guy’s got a rap sheet...'",
        "color": "orange",
        "image": '<a href="https://imgur.com/nMhqWyZ"><img src="https://i.imgur.com/nMhqWyZ.png" title="source: imgur.com" /></a>'
    },
    {
        key: 5,
        id: 'cyrus',
        "name": "Cyrus Morse",
        "Bio": " In his late 50s, groundskeeper to the Stone Manor for over twenty years. Manages a few estates in the area. Was incarcerated between 1992-1994. Gruff and quiet, he says he is ‘loyal to whoever signs his paycheck’.",
        "Alibi": " 'I ain't talkin' to you.'",
        "Motive": " Could this ex-con have slipped back into old habits?",
        "Guess": " 'I still ain't talkin' to you.'",
        "color": "yellow",
        "image": '<a href="https://imgur.com/Ar8PATX"><img src="https://i.imgur.com/Ar8PATX.png" title="source: imgur.com" /></a>'
    },
    {
        key: 6,
        id: 'francesca',
        "name": "Francesca Stone",
        "Bio": " The matriarch of the family. A strict disciplinarian, she considers ‘cold’ a compliment. Still, she’s always the biggest donor at fundraisers. She inherited the gems from her parents.",
        "Alibi": " 'I can prove the gems are my property, I wouldn’t steal what’s already mine.'",
        "Motive": " Could the insurance on the gems be more than they’re worth?",
        "Guess": " 'I can’t imagine who could have done this. I’m just shocked. My husband will tell you it couldn’t have been the girls, but he was always too easy on them.'",
        "color": "white",
        "image": "<a href='https://imgur.com/aTdLQrk'><img src='https://i.imgur.com/aTdLQrk.png' title='source: imgur.com' /></a>"
    },
    {
        key: 7,
        id: 'evie',
        "name": "Evie Stone",
        "Bio": " Middle daughter, a state lobbyist in Olympia. She’s given up the last few weekends to drive up and sit for the family portrait. She flipped her fair share of Monopoly boards when family game nights weren’t going her way.",
        "Alibi": " 'I was in the garage and ran up when I heard my mom scream.'",
        "Motive": " Despite living the furthest away, Evie helps her parents out the most. But maybe she feels that entitles her to more than her sisters...",
        "Guess": " 'Of course it was the artist! The vagrant just makes himself at home here. I’ll bet all the money in my wallet it was Leo.'",
        "color": "green",
        "image": '<a href="https://imgur.com/hXokkIg"><img src="https://i.imgur.com/hXokkIg.png" title="source: imgur.com" /></a>'
    },
    {
        key: 8,
        id: 'alexander',
        "name": "Alexander Stone",
        "Bio": " The patriarch of the family. He is notoriously kind, clever and giving. He's been collecting vintage typewriters for decades, and little else is known about his life before 'Frankie'. He's considerably younger looking than his wife, and has endured plenty of 'married-for-money' rumors.",
        "Alibi": " 'I was talking with our lawyer in the library.'",
        "Motive": " If he was to leave his wife, he’d need some money to get his new life started.",
        "Guess": " 'Of course the girls would never steal from us. And Cyrus is practically family. When did Beau get here?'",
        "color": "black",
        "image": '<a href="https://imgur.com/aJkNdjT"><img src="https://i.imgur.com/aJkNdjT.png" title="source: imgur.com" /></a>'
    }
];