import React from "react";
import "./Register.css";

function Register() {
    return (
        <div className="text-container">
            <h4 className="thank-you-message">This event has concluded. We are so grateful for all the feedback we've received. We are toying with the idea of a virtual round 2, but for now are basking in the compliments of our Halloween adventure.</h4>
        </div>
    )
}

export default Register;