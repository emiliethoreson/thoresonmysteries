import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Home from "./Home.js";
import AboutUs from "./Components/AboutUs";
import Register from "./Components/Register";
import SuspectsContainer from "./Components/SuspectsContainer";
import "./App.css";

function App() {

  return (
    <div className="App">
      <Router>
          <nav className="top-nav-bar">
            <ul className="main-nav">
              <li>
                <Link to="/">Home</Link>
              </li>
              <li>
                <Link to="/register">How do I join?</Link>
              </li>
              <li>
                <Link to="/about-us">What is this?</Link>
              </li>
              <li>
                <Link to="/suspects">Whodunnit?</Link>
              </li>
            </ul>
          </nav>
          <Switch>
            <Route path="/about-us">
              <AboutUs />
            </Route>
            <Route path="/register">
              <Register />
            </Route>
            <Route path="/suspects">
              <SuspectsContainer />
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
      </Router>
    </div>
  );
}

export default App;
