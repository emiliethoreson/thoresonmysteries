export const constraints = {
    emailAddress: {
      presence: {
        allowEmpty: false,
        message: "^field cannot be blank"
      },
      email: {
        message: "^please enter a valid email address"
      }
    },
    numberInParty: {
      numericality:{
        onlyInteger: true,
        greaterThan: 0,
        lessThan: 11,
        message: "^Number in party must be between 1-10"
      }
    }
  };
  
  export default constraints;