import React from "react";

function Home() {
      return (
        <div className="countdown-container">
          The Federal Way Halloween scavenger hunt is all wrapped up, and we want to give a huge thank you to the community. Thank you to our local businesses involved: Rhododendron Species Botanical Garden and Pacific Bonsai Museum, Federal Way Historical Society, Jump Start Espresso, and Blue Island Sushi & Roll. <br />An even bigger thank you to the people who heard about a scavenger hunt from a friend of a friend and took a chance on a vague description and a cryptic website. A few highlights:<br />
          <ul className="highlight-list">
            <li>When the first two teams at Redondo figured out "JUMP", they knew it was Jump Start Espresso. Early birds know their coffee!</li><br />
            <li>The two teachers who participated both guessed that "the kindergarten teacher must've done it". We'll try not to read into that too much.</li><br />
            <li>Shoutout to "Team Clever" for the fastest finish, a little over 3 hours including the 47 minutes of driving time!</li>
          </ul>
        </div>
      );
}

export default Home;